package com.mycompany.mavenproject111;
import com.mycompany.mavenproject111.Pictures;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
@WebServlet(name = "adminServlet", value = "/admin-servlet")
public class AdminServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        //String n = request.getParameter("username");
        out.print("<!DOCTYPE html> \n" +
                "  <html>\n");
        out.print("<span style=\"color: blue\"><center><hr>Welcome Admin! Which goods you want to add?</hr></center></span>");
        out.println(Pictures.buttonName("Add"));
        
        out.print( "</html>\n");
        out.close();
    }
}
